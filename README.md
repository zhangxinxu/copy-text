# 极简文字内容剪切板复制

### 介绍
带反馈提示的文字复制效果，无依赖，兼容现代浏览器，代码极简，即插即用。

演示：https://zhangxinxu.gitee.io/copy-text/demo.html

### 使用说明

1.  引用 JS 文件：
```html
<script src="./copyText.js"></script>
```
2.  绑定按钮和需要复制的内容
```js
// content 只能是字符串类型
copyText(content, callback);
// 或者
// content 此时也可以是 DOM 元素对象
copyText(button, content, callback);
```

#### 参数说明

<dl>
    <dt>button</dt>
    <dd>可选。Element。表示触发复制的按钮元素。</dd>
    <dt>content</dt>
    <dd>必须。String|Element。表示需要复制的内容。如果是 DOM 元素，则会使用内部的文本或者值作为复制内容。如果使用的是 <code>copyText(content)</code> 语法，content 只能是字符串。</dd>
    <dt>callback</dt>
    <dd>可选。Function。成功复制后的回调，支持一个参数，表示复制的文本内容。</dd>
</dl>

参数的具体使用效果参见 demo.html 。

### 更多

```copyText.tips(event)``` 方法可以调用内置的复制成功提示效果。

实现原理和其他说明参见这里：<a href="https://www.zhangxinxu.com/wordpress/?p=10150">https://www.zhangxinxu.com/wordpress/?p=10150</a>
